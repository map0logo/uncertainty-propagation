#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Propagación de Incertidumbre
Incremento de Superficie Forestal

Created on Sun Oct 18 18:37:24 2020

@author: fpalm
"""
from uncertainty_propagation import *

G = nx.DiGraph()
# --- Nodo final: no bosque a bosque
G.add_node("NBB")
G.add_node("NBB_Em")
G.add_edge("NBB_Em", "NBB")
# ---
G.add_node("NBB_DA")
# --- Datos de Actividad de Incremento
G.add_node("DA_Aum")
G.add_edge("DA_Aum", "NBB_DA")
# --- Datos de Actividad Restitución
G.add_node("DA_Res")
G.add_edge("DA_Res", "NBB_DA")
# --- Factor de Emisión Pre-Post
G.add_node("FE_Pre_Post")
# --- IPA por Cobertura
G.add_node("IPA_COB")
G.add_edge("IPA_COB", "FE_Pre_Post")
# --- No bosque a bosque, por región
G.add_node("NBB_Region")
# --- Área de no bosque a bosque, por región
G.add_node("NBB_Area")
G.add_edges_from(
    [
        ("FE_Pre_Post", "NBB_Region"),
        ("NBB_Area", "NBB_Region"),
    ]
)
# --- No bosque a bosque, actividad por región
G.add_node("NBB_DA_Region")
G.add_edges_from(
    [
        ("NBB_Region", "NBB_DA_Region"),
        ("NBB_DA", "NBB_DA_Region"),
    ]
)
G.add_edge("NBB_DA_Region", "NBB_Em")
# Incertidumbres por cobertura
G.add_node("COB")
# Incertidumbres por especie forestal
G.add_node("FE_IPA")
G.add_edges_from(
    [
        ("COB", "IPA_COB"),
        ("FE_IPA", "IPA_COB"),
    ]
)
# IPA por Especie Forestal
G.add_node("IPA")
# Factor de Emisión de Aumento por Especie Forestal 
G.add_node("FE_AU")
G.add_edge("IPA", "FE_IPA")
G.add_edge("FE_AU", "FE_IPA")
# Factores de Emisión - Parámetros
G.add_nodes_from(["DBM", "FR_BN", "FX_BN"])
G.add_edges_from(
    [
        ("DBM", "FE_AU"),
        ("FR_BN", "FE_AU"),
        ("FX_BN", "FE_AU"),
    ]
)

plot_graph(G, 15, 15)

# --- Cálculos de la Incertidumbre

G.nodes["IPA"]["unc"] = pd.read_csv(
    os.path.join(DATA_DIR, "post_ipa.csv"), index_col=0
)

G.nodes["DBM"]["unc"] = 5.6
G.edges[("DBM", "FE_AU")]["col"] = "Densidad básica de la madera"

G.nodes["FR_BN"]["unc"] = 40
G.edges[("FR_BN", "FE_AU")]["col"] = "Factor R bosque nativo"

G.nodes["FX_BN"]["unc"] = 18
G.edges[("FX_BN", "FE_AU")]["col"] = "Factor expansión bosque nativo"

index = G.nodes["IPA"]["unc"].index
columns = [
    "Densidad básica de la madera",
    "Factor R bosque nativo",
    "Factor expansión bosque nativo",
]

G.nodes["FE_AU"]["unc"] = None
G.nodes["FE_AU"]["fun"] = build_df
G.nodes["FE_AU"]["index"] = index
G.nodes["FE_AU"]["columns"] = columns

# print(f"FE_AU {calculate_uncertainty(G, 'FE_AU')}")

G.nodes["FE_IPA"]["unc"] = None
G.nodes["FE_IPA"]["fun"] = concat_dfs
G.nodes["FE_IPA"]["axis"] = 1

# print(f"FE_IPA {calculate_uncertainty(G, 'FE_IPA')}")

G.edges[("FE_IPA", "IPA_COB")]["pos"] = 0

G.nodes["COB"]["unc"] = pd.read_csv(
    os.path.join(DATA_DIR, "pre_error.csv"), index_col=0
)
G.edges[("COB", "IPA_COB")]["pos"] = 1

G.nodes["IPA_COB"]["unc"] = None
G.nodes["IPA_COB"]["fun"] = cross_dfs
G.nodes["IPA_COB"]["axis"] = 1

# print(f"IPA_COB {calculate_uncertainty(G, 'IPA_COB')}")

G.nodes["FE_Pre_Post"]["unc"] = None
G.nodes["FE_Pre_Post"]["fun"] = uncertainties_product
G.nodes["FE_Pre_Post"]["axis"] = 1

print(f"FE_Pre_Post {calculate_uncertainty(G, 'FE_Pre_Post')}")
# ---

G.nodes["DA_Aum"]["unc"] = pd.read_csv(
    os.path.join(DATA_DIR, "uarea_aum.csv"), index_col=0
)

G.nodes["DA_Aum"]["val"] = pd.read_csv(
    os.path.join(DATA_DIR, "area_aum.csv"), index_col=0
)

G.nodes["DA_Res"]["unc"] = pd.read_csv(
    os.path.join(DATA_DIR, "uarea_res.csv"), index_col=0
)

G.nodes["DA_Res"]["val"] = pd.read_csv(
    os.path.join(DATA_DIR, "area_res.csv"), index_col=0
)

G.nodes["NBB_DA"]["unc"] = None
G.nodes["NBB_DA"]["fun"] = uncertainties_sum
G.nodes["NBB_DA"]["axis"] = 1

print(f"NBB_DA {calculate_uncertainty(G, 'NBB_DA')}")

# ---

G.nodes["NBB_Area"]["val"] = pd.read_csv(
    os.path.join(DATA_DIR, "pre_post_area_redd.csv"), index_col=[1, 2]
)
G.edges[("NBB_Area", "NBB_Region")]["arg"] = "left"
G.edges[("FE_Pre_Post", "NBB_Region")]["arg"] = "right"         

G.nodes["NBB_Region"]["unc"] = None
G.nodes["NBB_Region"]["fun"] = merge_dfs
G.nodes["NBB_Region"]["index"] = "región"
G.nodes["NBB_Region"]["how"] = "left"
G.nodes["NBB_Region"]["on"] = ["pre", "post"]

print(f"NBB_Region {calculate_uncertainty(G, 'NBB_Region')}")

G.nodes["NBB_DA"]["val"] = pd.read_csv(
    os.path.join(DATA_DIR, "area_bosque_redd.csv"), index_col=[0]
)

G.nodes["NBB_DA_Region"]["unc"] = None
G.nodes["NBB_DA_Region"]["fun"] = concat_dfs2

print(f"NBB_DA_Region {calculate_uncertainty(G, 'NBB_DA_Region')}")

G.nodes["NBB_Em"]["unc"] = None
G.nodes["NBB_Em"]["fun"] = uncertainties_sum_apply
G.nodes["NBB_Em"]["index"] = "región"

print(f"NBB_Em {calculate_uncertainty(G, 'NBB_Em')}")

G.nodes["NBB_Em"]["val"] = pd.read_csv(
    os.path.join(DATA_DIR, "emisiones_nbb_redd.csv"), index_col=[0]
)

G.nodes["NBB"]["unc"] = None
G.nodes["NBB"]["fun"] = uncertainties_sum
G.nodes["NBB"]["axis"] = 0

print(f"NBB {calculate_uncertainty(G, 'NBB')}")
