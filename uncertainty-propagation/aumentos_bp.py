#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Propagación de Incertidumbre
Incremento de Bosque Permanente

Created on Thu Oct 22 14:38:35 2020

@author: fpalm
"""

from uncertainty_propagation import *

G = nx.DiGraph()

G.add_node("AuBP")
# ---
G.add_node("AuC")
G.add_edge("AuC", "AuBP")
# ---
G.add_node("AuBP_DA")
G.add_edge("AuBP_DA", "AuBP")
# ---
G.add_node("DA")
G.add_edge("DA", "AuBP_DA")
# ---
G.add_node("AuC")

G.add_nodes_from(["DAP", "FEx", "DM"])
G.add_edges_from(
    [
        ("DAP", "AuC"),
        ("FEx", "AuC"),
        ("DM", "AuC"),
    ]
)

G.add_node("VKNN")
G.add_nodes_from(["N_VKNN", "MEAN_VKNN", "STD_VKNN"])
G.add_edges_from([
    ("N_VKNN", "VKNN"),
    ("MEAN_VKNN", "VKNN"),
    ("STD_VKNN", "VKNN"),
])

G.add_edge("VKNN", "AuC")

plot_graph(G, 15, 15)

# --- Cálculos de la Incertidumbre

G.nodes["N_VKNN"]["val"] = 417
G.edges[("N_VKNN", "VKNN")]["arg"] = "n"

G.nodes["MEAN_VKNN"]["val"] = 320.52
G.edges[("MEAN_VKNN", "VKNN")]["arg"] = "mean"

G.nodes["STD_VKNN"]["val"] = 2280.7743408105944
# 111.69 * 417**0.5
G.edges[("STD_VKNN", "VKNN")]["arg"]= "std"

G.nodes["VKNN"]["unc"] = None
G.nodes["VKNN"]["fun"] = uncertainty_sample

# ---

G.nodes["DAP"]["unc"] = 0.2

G.nodes["FEx"]["unc"] = 18

G.nodes["DM"]["unc"] = 5.6

G.nodes["AuC"]["unc"] = None
G.nodes["AuC"]["fun"] = uncertainties_product
G.nodes["AuC"]["axis"] = 0

# ---

G.nodes["DA"]["unc"] = pd.read_csv(
    os.path.join(DATA_DIR, "uarea_bp.csv"), index_col=0
)

G.nodes["AuBP_DA"]["unc"] = None
G.nodes["AuBP_DA"]["fun"] = uncertainties_product
G.nodes["AuBP_DA"]["axis"] = 0

# ---

G.nodes["AuBP"]["unc"] = None
G.nodes["AuBP"]["fun"] = uncertainties_product
G.nodes["AuBP"]["axis"] = 0

print(f"AuBP {calculate_uncertainty(G, 'AuBP')}")