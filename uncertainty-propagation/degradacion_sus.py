#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Propagación de Incertidumbre
Degradación por sustitución

Created on Thu Oct 22 14:37:01 2020

@author: fpalm
"""

from uncertainty_propagation import *

G = nx.DiGraph()

G.add_node("SUS")

G.add_node("SUS_Em")
G.add_edge("SUS_Em", "SUS")

G.add_nodes_from(["SUS_DA", "SUS_BN", "SUS_MA"])
G.add_edges_from(
    [("SUS_DA", "SUS_Em"), ("SUS_BN", "SUS_Em"), ("SUS_MA", "SUS_Em")]
)

G.add_node("DA")
G.add_edge("DA", "SUS_DA")

G.add_node("BIOM_MA")
G.add_edge("BIOM_MA", "SUS_MA")

G.add_nodes_from(["BIOM_Ae_MA", "FR_MA"])
G.add_edges_from([("BIOM_Ae_MA", "BIOM_Sub_MA"), ("FR_MA", "BIOM_Sub_MA")])

G.add_edges_from([("BIOM_Sub_MA", "BIOM_MA"), ("BIOM_Ae_MA", "BIOM_MA")])

G.add_node("BIOM_BN")
G.add_edge("BIOM_BN", "SUS_BN")
# ---
G.add_nodes_from(["NECR_Pie", "NECR_Suelo"])
G.add_edges_from(
    [
        ("NECR_Pie", "BIOM_BN"),
        ("NECR_Suelo", "BIOM_BN"),
        ("NECR_Pie", "BIOM_MA"),
        ("NECR_Suelo", "BIOM_MA"),
    ]
)

G.add_nodes_from(["BIOM_Ae_BN", "FR_BN"])
G.add_edge("BIOM_Ae_BN", "BIOM_BN")
G.add_edges_from([("BIOM_Ae_BN", "BIOM_Sub_BN"), ("FR_BN", "BIOM_Sub_BN")])

G.add_node("BIOM_Sub_BN")
G.add_edge("BIOM_Sub_BN", "BIOM_BN")


G.add_nodes_from(["DAP", "VOL", "FEx", "DM"])
G.add_edges_from(
    [
        ("DAP", "BIOM_Ae_BN"),
        ("VOL", "BIOM_Ae_BN"),
        ("FEx", "BIOM_Ae_BN"),
        ("DM", "BIOM_Ae_BN"),
    ]
)
# ---
G.add_nodes_from(["N_TREE", "MEAN_TREE", "STD_TREE"])
G.add_edges_from(
    [("N_TREE", "VOL"), ("MEAN_TREE", "VOL"), ("STD_TREE", "VOL")]
)

plot_graph(G, 15, 15)

# --- Cálculos de la Incertidumbre

G.nodes["N_TREE"]["val"] = 6961
G.edges[("N_TREE", "VOL")]["arg"] = "n"

G.nodes["MEAN_TREE"]["val"] = 0.3106
G.edges[("MEAN_TREE", "VOL")]["arg"] = "mean"

G.nodes["STD_TREE"]["val"] = 0.009897
G.edges[("STD_TREE", "VOL")]["arg"] = "std"

G.nodes["VOL"]["unc"] = None
G.nodes["VOL"]["fun"] = uncertainty_sample

# print(f"VOL {calculate_uncertainty(G, 'VOL')}")

# ---

G.nodes["DAP"]["unc"] = 0.2

G.nodes["FEx"]["unc"] = 18

G.nodes["DM"]["unc"] = 5.6

G.nodes["BIOM_Ae_BN"]["unc"] = None
G.nodes["BIOM_Ae_BN"]["fun"] = uncertainties_product
G.nodes["BIOM_Ae_BN"]["axis"] = 0

# print(f"BIOM_Ae_BN {calculate_uncertainty(G, 'BIOM_Ae_BN')}")

# ---
G.nodes["FR_BN"]["unc"] = 40

G.nodes["BIOM_Sub_BN"]["unc"] = None
G.nodes["BIOM_Sub_BN"]["fun"] = uncertainties_product
G.nodes["BIOM_Sub_BN"]["axis"] = 0

# print(f"BIOM_Sub_BN {calculate_uncertainty(G, 'BIOM_Sub_BN')}")

# ---
G.nodes["BIOM_Ae_MA"]["unc"] = 22.4225

G.nodes["FR_MA"]["unc"] = 48.27

G.nodes["BIOM_Sub_MA"]["unc"] = None
G.nodes["BIOM_Sub_MA"]["fun"] = uncertainties_product
G.nodes["BIOM_Sub_MA"][
    "axis"
] = 0  # para que aplique la función como una fila.

# print(f"BIOM_Sub_MA {calculate_uncertainty(G, 'BIOM_Sub_MA')}")

# ---
G.edges[("BIOM_Ae_BN", "BIOM_BN")]["col"] = "Biomasa aérea"

G.edges[("BIOM_Sub_BN", "BIOM_BN")]["col"] = "Biomasa subterránea"

# Error estimado de las parcelas permanentes del Inventario forestal continúo del INFOR.
G.nodes["NECR_Pie"]["unc"] = 28.4
G.edges[("NECR_Pie", "BIOM_BN")]["col"] = "Necromasa Pie"

# Error estimado de las parcelas permanentes del Inventario forestal continúo del INFOR.
G.nodes["NECR_Suelo"]["unc"] = 24.17
G.edges[("NECR_Suelo", "BIOM_BN")]["col"] = "Necromasa Suelo"


index = ["Maule", "Bío Bío", "La Araucanía", "Los Ríos", "Los Lagos"]
columns = [
    "Biomasa Aérea",
    "Biomasa Subterránea",
    "Necromasa Pie",
    "Necromasa Suelo",
]


G.edges[("BIOM_Ae_BN", "BIOM_BN")]["col"] = "Biomasa Aérea"
G.edges[("BIOM_Sub_BN", "BIOM_BN")]["col"] = "Biomasa Subterránea"

G.nodes["BIOM_BN"]["unc"] = None
G.nodes["BIOM_BN"]["fun"] = build_df
G.nodes["BIOM_BN"]["index"] = index
G.nodes["BIOM_BN"]["columns"] = columns

# print(f"BIOM_BN {calculate_uncertainty(G, 'BIOM_BN')}")

# ---

G.nodes["BIOM_BN"]["val"] = pd.read_csv(
    os.path.join(DATA_DIR, "biomasa.csv"), index_col=0
)

G.nodes["SUS_BN"]["unc"] = None
G.nodes["SUS_BN"]["fun"] = uncertainties_sum
G.nodes["SUS_BN"]["axis"] = 1

# print(f"SUS_BN {calculate_uncertainty(G, 'SUS_BN')}")

# ---
G.nodes["DA"]["val"] = pd.read_csv(
    os.path.join(DATA_DIR, "area_sus.csv"), index_col=0
)
G.nodes["DA"]["unc"] = (
    pd.read_csv(os.path.join(DATA_DIR, "uarea_sus.csv"), index_col=0) / 100
)

G.nodes["SUS_DA"]["unc"] = None
G.nodes["SUS_DA"]["fun"] = uncertainties_sum
G.nodes["SUS_DA"]["type"] = "df"
G.nodes["SUS_DA"]["axis"] = 1

# print(f"SUS_DA {calculate_uncertainty(G, 'SUS_DA')}")
# ---

index = ["Maule", "Bío Bío", "La Araucanía", "Los Ríos", "Los Lagos"]
columns = [
    "Biomasa Aérea",
    "Biomasa Subterránea",
    "Necromasa Pie",
    "Necromasa Suelo",
]

G.edges[("BIOM_Ae_MA", "BIOM_MA")]["col"] = "Biomasa Aérea"
G.edges[("BIOM_Sub_MA", "BIOM_MA")]["col"] = "Biomasa Subterránea"

# Error estimado de las parcelas permanentes del Inventario forestal continúo del INFOR.
G.edges[("NECR_Pie", "BIOM_MA")]["col"] = "Necromasa Pie"

# Error estimado de las parcelas permanentes del Inventario forestal continúo del INFOR.
G.edges[("NECR_Suelo", "BIOM_MA")]["col"] = "Necromasa Suelo"


G.nodes["BIOM_MA"]["unc"] = None
G.nodes["BIOM_MA"]["fun"] = build_df
G.nodes["BIOM_MA"]["index"] = index
G.nodes["BIOM_MA"]["columns"] = columns

# print(f"BIOM_MA {calculate_uncertainty(G, 'BIOM_MA')}")

# ---

G.nodes["BIOM_MA"]["val"] = pd.read_csv(
    os.path.join(DATA_DIR, "biomasa_ma.csv"), index_col=0
)

G.nodes["SUS_MA"]["unc"] = None
G.nodes["SUS_MA"]["fun"] = uncertainties_sum
G.nodes["SUS_MA"]["axis"] = 1

# print(f"SUS_MA {calculate_uncertainty(G, 'SUS_MA')}")

# ---

G.nodes["SUS_BN"]["unc"] = None
G.nodes["SUS_BN"]["fun"] = uncertainties_sum
G.nodes["SUS_BN"]["axis"] = 1

# print(f"SUS_BN {calculate_uncertainty(G, 'SUS_BN')}")

# ---

G.nodes["SUS_Em"]["unc"] = None
G.nodes["SUS_Em"]["fun"] = uncertainties_product
G.nodes["SUS_Em"]["axis"] = 1

print(f"SUS_Em {calculate_uncertainty(G, 'SUS_Em')}")

# ---

G.nodes["SUS_Em"]["val"] = pd.read_csv(
    os.path.join(DATA_DIR, "emisiones_sus.csv"), names=[0], index_col=0
)

G.nodes["SUS"]["unc"] = None
G.nodes["SUS"]["fun"] = uncertainties_sum
G.nodes["SUS"]["axis"] = 0

print(f"SUS {calculate_uncertainty(G, 'SUS')}")
