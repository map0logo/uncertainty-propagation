#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  2 10:31:15 2020

@author: fpalm
"""

G.add_node("TOT")
G.add_edges(
    [
        ("DEF", "TOT"),
        ("SUS", "TOT"),
        ("INC", "TOT"),
        ("DEG_BP", "TOT"),
        ("NBB", "TOT"),
        ("AuBP", "TOT"),
        ("CON", "TOT"),
    ]
)

G.nodes["DEF"]["val"] = 21.6905326582638
G.nodes["SUS"]["val"] = 14.8824651503561
G.nodes["INC"]["val"] = 26.8896421206139
G.nodes["DEG_BP"]["val"] = 83.0273427173031
G.nodes["NBB"]["val"] = 7.89041391378208
G.nodes["AuBP"]["val"] = 71.2296278706055
G.nodes["CON"]["val"] = 65.2687123189447



G.nodes["TOT"]["unc"] = None
G.nodes["TOT"]["fun"] = uncertainty_sum

print(f"TOT {calculate_uncertainty(G, 'TOT')}")