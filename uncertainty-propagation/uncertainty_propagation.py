#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 13 07:43:51 2020

@author: fpalm
"""

import os.path
import pathlib
from collections import defaultdict

import networkx as nx
import numpy as np
import pandas as pd
from scipy import stats
import matplotlib.pyplot as plt

BASE_DIR = pathlib.Path(__file__).parent.parent.absolute()
DATA_DIR = os.path.join(BASE_DIR, "data")


def cast_args(node, args, key):
    """
    Ajusta los tipos de los argumentos en caso de ser necesario.

    Parameters
    ----------
    node : dict
        Nodo actual del grafo.
    args : dict of [list | numpy.array | pandas.DataFrame] 
        Valores de entrada.
    key : int
        Clave correspondiente al argumento de la función a evaluar.

    Raises
    ------
    
        TypeError, en caso que no todos los elementos sean del mismo tipo.
        No se admiten valores de entrada que sean mezcla de tablas y valores
        individuales.

    Returns
    -------
    args : dict
        Diccionario de los argumentos de la función.

    """
    if all(isinstance(x, (int, float)) for x in args[key]):
        args[key] = np.array(args[key])
    elif all(isinstance(x, pd.DataFrame) for x in args[key]):
        args[key] = pd.concat(args[key], axis=1)
    else:
        raise (f"TypeError mixed input types in {node}:{key}")
    return args


def uncertainty_sample(n, mean, std, q=0.975):
    """
    Calcula la incertidumbre de una muestra estadística.

    Parameters
    ----------
    n : int
        Tamaño de la muestra.
    mean : float
        Media muestral.
    std : float
        Desviación estándar.
    q : float, optional
        Cuantil de una distribción normal estándar. El valor por defecto es
        0.975, correspondiente a un intervalor de confianza del 95%.

    Returns
    -------
    float
        Incertidumbre porcentual de la muestra.

    """
    return (stats.norm.ppf(q) * std / np.sqrt(n)) / mean * 100


def uncertainty_sample_args(G, node):
    """
    Construye los argumentos para el cálculo de la incertidumbre de una muestra
    estadística.

    Parameters
    ----------
    G : networkx.DiGraph
        Grafo del modelo de incertidumbre.
    node : dict
        Nodo desde donde se va a realizar el cálculo.

    Raises
    ------

        "Missing {child} value" cuando falta el valor en uno de los nodos hijo.

    Returns
    -------
    args : dict
        Un diccionario con los valores para evaluar la función del nodo.

    """
    args = {}
    for child in G.predecessors(node):
        if G.nodes[child]["val"] is None:
            raise (f"Missing {child} value")
        args[G.edges[(child, node)]["arg"]] = G.nodes[child]["val"]
    return args


def uncertainties_product(uncertainties, axis=1):
    """
    Incertidumbre de un producto. Esta fórmula es una versión vectorizada de la
    formula 3.1 del IPCC 2006.
    
    .. math::

       U_{TOTAL} = \sqrt{U_1^2 + U_2^2 + \dots + U_n^2}

    Parameters
    ----------
    uncertainties : pandas.DataFrame o numpy.array
        Tabla de datos (DataFrame) o arreglo con los valores de las
        incertidumbres de un conjunto de variables que se multiplican entre si.
        Las incertidumbres deben estar expresadas en porcentajes o fracciones.
    axis : int, optional
        Dirección en que se realiza la operación, ya sea a lo largo de las
        filas (axis=0) o de columnas (axis=1). El valor por defecto es 1.

    Returns
    -------
    pandas.DataFrame o numpy.array
        Tabla de datos o arreglo con los valores de las incertidumbres.

    """
    return np.sqrt(np.sum(uncertainties ** 2, axis))


def uncertainties_product_args(G, node):
    """
    Construye los argumentos para el cálculo de la incertidumbre de un
    producto.

    Parameters
    ----------
    G : networkx.DiGraph
        Grafo del modelo de incertidumbre.
    node : dict
        Nodo desde donde se va a realizar el cálculo.

    Returns
    -------
    args : dict of list
        Un diccionario con los valores para evaluar la función del nodo.

    """
    args = defaultdict(list)
    for child in G.predecessors(node):
        if G.nodes[child]["unc"] is None:
            calculate_uncertainty(G, child)
        args["uncertainties"].append(G.nodes[child]["unc"])
    args = cast_args(node, args, "uncertainties")
    if "axis" in G.nodes[node].keys():
        args["axis"] = G.nodes[node]["axis"]
    return args


def uncertainties_sum(values, uncertainties, axis=1):
    """
    Incertidumbre de una suma. Esta fórmula es una versión vectorizada de la
    fórmula 3.2 del IPCC 2006.
    
    .. math::

       U_{TOTAL} = \\frac{\\sqrt{(U_1 \\bullet x_1)^2 + (U_2 \\bullet x_2)^2 + \\dots + (U_n \\bullet x_n)^2}}{|x_1 + x_2 + \\dots + x_n|}
    
    Parameters
    ----------
    values : pandas.DataFrame o numpy.array
        Tabla de datos (DataFrame) o arreglo con los valores de un conjunto de
        variables que se suman entre si.
    uncertainties : pandas.DataFrame o numpy.array
        Tabla de datos (DataFrame) o arreglo con los valores de las
        incertidumbres de un conjunto de variables que se suman entre si.
        Las incertidumbres deben estar expresadas en porcentajes o fracciones.
    axis : int, optional
        Dirección en que se realiza la operación, ya sea a lo largo de las
        filas (axis=0) o de columnas (axis=1). El valor por defecto es 1.

    Returns
    -------
    pandas.DataFrame o numpy.array
        Tabla de datos o arreglo con los valores de las incertidumbres.

    """
    return np.sqrt(np.sum((values * uncertainties) ** 2, axis)) / np.abs(
        np.sum(values, axis)
    )


def uncertainties_sum_args(G, node, df=True):
    """
    Construye los argumentos para el cálculo de la incertidumbre de una suma.

    Parameters
    ----------
    G : networkx.DiGraph
        Grafo del modelo de incertidumbre.
    node : dict
        Nodo desde donde se va a realizar el cálculo.
    df : boolean, optional
        DESCRIPTION. The default is True.

    Returns
    -------
    args : dict of list
        Un diccionario con los valores para evaluar la función del nodo.

    """
    args = defaultdict(list)
    childs = list(G.predecessors(node))
    if len(childs) == 1:
        child = childs[0]
        args["values"] = G.nodes[child]["val"]
        if G.nodes[child]["unc"] is None:
            calculate_uncertainty(G, child)
        args["uncertainties"] = G.nodes[child]["unc"]
        args["values"].columns = args["uncertainties"].columns
    else:
        for child in G.predecessors(node):
            if G.nodes[child]["unc"] is None:
                calculate_uncertainty(G, child)
            args["values"].append(G.nodes[child]["val"])
            args["uncertainties"].append(G.nodes[child]["unc"])
        args = cast_args(node, args, "values")
        args = cast_args(node, args, "uncertainties")
    if "axis" in G.nodes[node].keys():
        args["axis"] = G.nodes[node]["axis"]
    return args


def build_df(row, index, columns):
    """
    Construir tabla a partir de valores individuales.     

    Parameters
    ----------
    row : numpy.array
        Arreglo con los valores que se corresponden a una lista de valores.
    index : list
        Identificadores de las filas.
    columns : list
        Identificadores de las columnas.

    Returns
    -------
    pandas.DataFrame
        Valores convertidos a una tabla de valores.

    """
    return pd.DataFrame(
        np.resize(row, (len(index), len(columns))),
        index=index,
        columns=columns,
    )


def build_df_args(G, node):
    """
    Construye los argumentos de la función para construir tablas.

    Parameters
    ----------
    G : networkx.DiGraph
        Grafo del modelo de incertidumbre.
    node : dict
        Nodo desde donde se va a realizar el cálculo.

    Returns
    -------
    args : dict of list
        Un diccionario con los valores para evaluar la función del nodo.

    """
    args = defaultdict(list)
    cols = defaultdict(list)
    for child in G.predecessors(node):
        if G.nodes[child]["unc"] is None:
            calculate_uncertainty(G, child)
        args["row"].append(G.nodes[child]["unc"])
        cols["col"].append(G.edges[(child, node)]["col"])
    args["index"] = G.nodes[node]["index"]
    args["columns"] = G.nodes[node]["columns"]
    args["row"] = [args["row"][cols["col"].index(col)] for col in args["columns"]]
    return args


def concat_dfs(row, axis=1):
    """
    Concatena (une) tablas de valores.

    Parameters
    ----------
    row : list
        Es una secuencia de tablas a ser unidas.
    axis : int, optional
        Dirección en que se realiza la operación, ya sea a lo largo de las
        filas (axis=0) o de columnas (axis=1). El valor por defecto es 1.
        
    Returns
    -------
    pandas.DataFrame o pandas.Series
        Cuando concatena tablas de una columna a lo largo de filas devuelve
        una Serie (una columna de tabla), cuando une tablas a lo largo de
        varias columnas (axis=1) o tablas de varias columnas a lo largo de
        filas (axis=0) devuelve una tabla de datos.

    """
    return pd.concat(row, axis)


def concat_dfs_args(G, node):
    """
    Construye los argumentos de la función para unir tablas de valores.

    Parameters
    ----------
    G : networkx.DiGraph
        Grafo del modelo de incertidumbre.
    node : dict
        Nodo desde donde se va a realizar el cálculo.

    Returns
    -------
    args : dict of list
        Un diccionario con los valores para evaluar la función del nodo.

    """
    args = defaultdict(list)
    for child in G.predecessors(node):
        if G.nodes[child]["unc"] is None:
            calculate_uncertainty(G, child)
        args["row"].append(G.nodes[child]["unc"])
    if "axis" in G.nodes[node].keys():
        args["axis"] = G.nodes[node]["axis"]
    return args


def cross_dfs(row, axis=1):
    """
    Cruza tablas. Esto es, a partir de dos tablas, crea una nueva tabla
    copiando los valores de ambas tablas en una nueva tabla cuyo índice es el
    producto cartesiano de los índices de las tablas de entrada. Ver ejemplo.

    Parameters
    ----------
    row : list
        Una secuencia de tablas (pandas.DataFrame) cuyos índices tienen nombre.
    axis : int, optional
        Dirección en que se van a crear los índices jerárquicos. El valor por
        defecto es 1, en este caso se crea un índice columna, cuando
        `axis=0` se crearía un índice jerárquico fila. 

    Returns
    -------
    df5 : pandas.DataFrame
        La tabla resultante de cruzar las tablas de entrada.
        
    Examples
    --------
    >>> df1 = pd.DataFrame([["a", 1], ["b", 2]], columns=["letter", "l-val"])
    >>> df1.set_index("letter", inplace=True)
    >>> df1
            l-val
    letter       
    a           1
    b           2
    >>> df2 = pd.DataFrame([['1', 3], ['2', 4]], columns=['digit', 'd-val'])
    >>> df2.set_index("digit", inplace=True)
    >>> df2
           d-val
    digit       
    1          3
    2          4
    >>> row = [df1, df2]
    >>> result = cross_dfs(row)
    >>> result
                  d-val  l-val
    digit letter              
    1     a           3      1
          b           3      2
    2     a           4      1
          b           4      2
    

    """
    assert len(row) == 2
    df1 = row[0]
    df2 = row[1]
    df3 = df2.reindex(df2.index.repeat(len(df1.index)))
    df4 = df1.reindex(list(df1.index) * len(df2.index))
    df5 = pd.concat([df3.reset_index(), df4.reset_index()], axis=axis)
    df5.set_index([df2.index.name, df1.index.name], inplace=True)
    return df5


def cross_dfs_args(G, node):
    """
    Construye los argumentos de la función para cruzar tablas.

    Parameters
    ----------
    G : networkx.DiGraph
        Grafo del modelo de incertidumbre.
    node : dict
        Nodo desde donde se va a realizar el cálculo.

    Returns
    -------
    args : dict of list
        Un diccionario con los valores para evaluar la función del nodo.

    """
    args = defaultdict(list)
    poss = []
    for child in G.predecessors(node):
        if G.nodes[child]["unc"] is None:
            calculate_uncertainty(G, child)
        args["row"].append(G.nodes[child]["unc"])
        poss.append(G.edges[(child, node)]["pos"])
    args["row"] = [args["row"][pos] for pos in poss]
    if "axis" in G.nodes[node].keys():
        args["axis"] = G.nodes[node]["axis"]
    return args


def merge_dfs(left, right, index=None, **kwargs):
    """
    Mezcla dos tablas con una unión (join) de tipo base de datos. Básicamente
    busca las incertidumbres que están en la tabla de la derecha `right`,
    correspondientes a los índices de la tabla de la izquierda `left`. Y
    devuelve los resultados con los índices indicados en "index".

    Parameters
    ----------
    left : pandas.DataFrame
        Tabla a mezclar.
    right : pandas.DataFrame
        Tabla con la que se mezcla.
    index : str, optional
        Nombre de la columna común a utilizar como índice. El valor por defecto
        es None.
    **kwargs : unpacked dict
        Parámetros adicionales para la unión, puede ser "how" que indica el
        tipo de unión, "on" para indicar por cuál columna común mezclarlos.
        O para mezclarlos por columnas distintas en cada tabla, se utiliza
        "left_on" para indicar la columna o columnas de la tabla `left`, y
        "right_on" para indicar la columna o columnas de la tabla `right`.

    Returns
    -------
    df1 : pandas.DataFrame
        Tabla con los valores mezclados, indexados por "index".
    df2 : pandas.DataFrame
        Tabla con las incertidumbres mezcladas, indexados por "index".
        
    Examples
    --------
    >>> val = pd.DataFrame(
    ...     [
    ...         ["a", "1", "azul", 1],
    ...         ["b", "1", "rojo", 2],
    ...         ["a", "1", "rojo", 3],
    ...         ["b", "2", "azul", 4],
    ...         ["a", "2", "rojo", 5],
    ...         ["b", "1", "rojo", 6],
    ...     ],
    ...     columns=["i1", "i2", "i3", "val"],
    ... )
    >>> val.set_index(["i1", "i2"], inplace=True)
             i3  val
    i1 i2           
    a  1   azul    1
    b  1   rojo    2
    a  1   rojo    3
    b  2   azul    4
    a  2   rojo    5
    b  1   rojo    6
    >>> unc = pd.DataFrame(
    ...     [
    ...         ["a", "1", 0.1],
    ...         ["b", "1", 0.2],
    ...         ["a", "2", 0.3],
    ...         ["b", "2", 0.4]
    ...     ],
    ...     columns=["i1", "i2", "val"],
    ... )
    >>> unc.set_index(["i1", "i2"], inplace=True)
           val
    i1 i2     
    a  1   0.1
    b  1   0.2
    a  2   0.3
    b  2   0.4
    >>> rval, runc = merge_dfs(val, unc, index="i3", on=["i1", "i2"])
    >>> rval
          val
    i3       
    azul    1
    rojo    3
    rojo    2
    rojo    6
    azul    4
    rojo    5
    >>> runc
          val
    i3       
    azul  0.1
    rojo  0.1
    rojo  0.2
    rojo  0.2
    azul  0.4
    rojo  0.3

    """
    df = left.merge(right, **kwargs)
    if index:
        df.set_index(index, inplace=True)
    df1 = df.loc[:, "val_x"].to_frame()
    df1.columns = ["val"]
    df2 = df.loc[:, "val_y"].to_frame()
    df2.columns = ["val"]
    return df1, df2


def merge_dfs_args(G, node):
    """
    Construye los argumentos de la función para construir tablas.

    Parameters
    ----------
    G : networkx.DiGraph
        Grafo del modelo de incertidumbre.
    node : dict
        Nodo desde donde se va a realizar el cálculo.

    Returns
    -------
    args : dict
        Un diccionario con los valores para evaluar la función del nodo.

    """
    args = {}
    for child in G.predecessors(node):
        side = G.edges[(child, node)]["arg"]
        args[side] = G.nodes[child]["val" if side == "left" else "unc"]
    for key in G.nodes[node]:
        if not key in ["unc", "val", "fun"]:
            args[key] = G.nodes[node][key]
    return args


def concat_dfs2(val, unc, axis=0):
    """
    Concatena (une) simultáneamente tablas de valores e incertidumbres a lo
    largo del eje "axis".

    Parameters
    ----------
    val : list of pandas.DataFrame
        Lista de tablas de valores.
    unc : list of pandas.DataFrame
        Lista de tablas de incertidumbres.
    axis : int, optional
        Dirección en que se unen las tablas. El valor por defecto es 0, en este
        caso se unen a lo largo de las filas, cuando `axis=1` se unen a lo
        largo de las columnas. 

    Returns
    -------
    df1 : pandas.DataFrame
        Tabla con los valores unidos.
    df2 : pandas.DataFrame
        Tabla con las incertidumbres unidas.

    """
    df1 = pd.concat(val, axis)
    df2 = pd.concat(unc, axis)
    return df1, df2


def concat_dfs2_args(G, node):
    """
    Construye los argumentos de la función para unir simultáneamente tablas de
    valores e incertidumbres.

    Parameters
    ----------
    G : networkx.DiGraph
        Grafo del modelo de incertidumbre.
    node : dict
        Nodo desde donde se va a realizar el cálculo.

    Returns
    -------
    args : dict of list
        Un diccionario con los valores para evaluar la función del nodo.

    """
    args = defaultdict(list)
    for child in G.predecessors(node):
        args["val"].append(G.nodes[child]["val"])
        args["unc"].append(G.nodes[child]["unc"])
    if "axis" in G.nodes[node].keys():
        args["axis"] = G.nodes[node]["axis"]
    return args


def uncertainties_sum_apply(grouped, axis=0):
    """
    Aplica la función de la incertidumbre de la suma por grupos a lo largo de
    un eje.

    Parameters
    ----------
    grouped : DataFrameGroupBy
        Valores agrupados por un eje.
    axis : int, optional
        Dirección en que se aplica la función. El valor por defecto es 0, en
        este caso se aplica a lo largo de las filas, cuando `axis=1` se aplica
        a lo largo de las columnas.

    Returns
    -------
    pandas.DataFrame
        Tabla con los valores de la incertidumbre de la suma por grupos.

    """
    def uncertainties_sum_group(group, axis=0):
        values = group.loc[:, "val"].values
        uncertainties = group.loc[:, "unc"].values
        return uncertainties_sum(values, uncertainties, axis)

    return grouped.apply(uncertainties_sum_group)


def uncertainties_sum_apply_args(G, node):
    """
    Construye los argumentos de la función para aplicar la función de la
    incertidumbre de la suma.

    Parameters
    ----------
    G : networkx.DiGraph
        Grafo del modelo de incertidumbre.
    node : dict
        Nodo desde donde se va a realizar el cálculo.

    Returns
    -------
    args : dict of list
        Un diccionario con los valores para evaluar la función del nodo.

    """
    args = {}
    for child in G.predecessors(node):
        val = G.nodes[child]["val"]
        unc = G.nodes[child]["unc"]
    df = pd.concat([val, unc], axis=1)
    df.columns = ["val", "unc"]
    args["grouped"] = df.groupby(G.nodes[node]["index"])
    if "axis" in G.nodes[node].keys():
        args["axis"] = G.nodes[node]["axis"]
    return args


def calculate_uncertainty(G, node):
    """
    Aplica recursivamente la operaciones de cálculo de la incertidumbre sobre
    un grafo.

    Parameters
    ----------
    G : networkx.DiGraph
        Grafo del modelo de incertidumbre.
    node : dict
        Nodo desde donde se va a realizar el cálculo.

    Returns
    -------
    float o pandas.DataFrame
        Valor de la incertidumbre en el nodo.

    """
    if not (G.nodes[node]["unc"] is None):  # está calculado
        return G.nodes[node]["unc"]
    if G.nodes[node]["fun"] == uncertainty_sample:
        args = uncertainty_sample_args(G, node)
    if G.nodes[node]["fun"] == uncertainties_product:
        args = uncertainties_product_args(G, node)
    if G.nodes[node]["fun"] == uncertainties_sum:
        args = uncertainties_sum_args(G, node)
    if G.nodes[node]["fun"] == build_df:
        args = build_df_args(G, node)
    if G.nodes[node]["fun"] == concat_dfs:
        args = concat_dfs_args(G, node)
    if G.nodes[node]["fun"] == cross_dfs:
        args = cross_dfs_args(G, node)
    if G.nodes[node]["fun"] == merge_dfs:
        args = merge_dfs_args(G, node)
    if G.nodes[node]["fun"] == concat_dfs2:
        args = concat_dfs2_args(G, node)
    if G.nodes[node]["fun"] == uncertainties_sum_apply:
        args = uncertainties_sum_apply_args(G, node)
    if G.nodes[node]["fun"] in [merge_dfs, concat_dfs2]:
        G.nodes[node]["val"], G.nodes[node]["unc"] = G.nodes[node]["fun"](**args)
    else:
        G.nodes[node]["unc"] = G.nodes[node]["fun"](**args)
    if isinstance(G.nodes[node]["unc"], pd.Series):
        G.nodes[node]["unc"] = G.nodes[node]["unc"].to_frame()
        G.nodes[node]["unc"].columns = ["val"]
    if isinstance(G.nodes[node]["unc"], pd.DataFrame):
        df = G.nodes[node]["unc"]
        if len(df.index) == 1 and len(df.columns) == 1:
            G.nodes[node]["unc"] = df.squeeze()
    return G.nodes[node]["unc"]


def plot_graph(G, width, height):
    """
    Genera un grafo que representa gráficamente el modelo de incertidumbre.

    Parameters
    ----------
    G : networkx.DiGraph
        Grafo del modelo de incertidumbre.
    width : float
        Ancho de la imagen en pulgadas.
    height : float
        Alto de la imagen en pulgadas.

    Returns
    -------
    None.

    """
    plt.figure(figsize=(width, height), dpi=100)
    plt.margins(0.15)
    pos = nx.drawing.nx_agraph.graphviz_layout(G, prog="dot")
    nx.draw_networkx_nodes(G, pos, node_size=500)
    nx.draw_networkx_edges(G, pos, arrows=True, width=2, edge_color="#888888")
    labels = nx.draw_networkx_labels(G, pos, font_size=9, font_color="#000000")
