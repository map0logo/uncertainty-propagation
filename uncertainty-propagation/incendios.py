#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Propagación de Incertidumbre
Degradación por incendios

Created on Thu Oct 22 14:39:11 2020

@author: fpalm
"""

from uncertainty_propagation import *

G = nx.DiGraph()

G.add_node("INC")
# ---
G.add_node("Sup_INC")
G.add_edge("Sup_INC", "INC")
# ---
G.add_node("Area_RE")
G.add_edge("Area_RE", "INC")
# ---
G.add_node("F_Com")
G.add_edge("F_Com", "Area_RE")
# ---
G.add_nodes_from(["FE_Gases", "M_Com"])
G.add_edges_from([("FE_Gases", "F_Com"), ("M_Com", "F_Com")])
# ---
G.add_node("Gases")
G.add_edge("Gases", "FE_Gases")
# ---
G.add_node("Com_Reg_T")
G.add_edge("Com_Reg_T", "F_Com")
# ---
G.add_node("Com_Reg")
G.add_edge("Com_Reg", "Com_Reg_T")
# ---
G.add_nodes_from(["Em_CH4", "Em_N2O"])
G.add_edges_from([
    ("Em_CH4", "Gases"),
    ("Em_N2O", "Gases")
])
# ---
G.add_node("BIOM_Com")
G.add_edge("BIOM_Com", "M_Com")
# ---
G.add_node("BIOM_Ae_BN")
G.add_edge("BIOM_Ae_BN", "BIOM_Ae_Com")
# ---
G.add_node("NECR_Com")
# ---
G.add_nodes_from(["NECR_Pie", "NECR_Suelo"])
G.add_edges_from([("NECR_Pie", "NECR_Com"), ("NECR_Suelo", "NECR_Com")])
# ---
G.add_edges_from([("NECR_Com", "BIOM_Com"), ("BIOM_Ae_Com", "BIOM_Com")])
# ---
G.add_nodes_from(["DAP", "VOL", "FEx", "DM"])
G.add_edges_from(
    [
        ("DAP", "BIOM_Ae_BN"),
        ("VOL", "BIOM_Ae_BN"),
        ("FEx", "BIOM_Ae_BN"),
        ("DM", "BIOM_Ae_BN"),
    ]
)
# ---
G.add_nodes_from(["N_TREE", "MEAN_TREE", "STD_TREE"])
G.add_edges_from(
    [("N_TREE", "VOL"), ("MEAN_TREE", "VOL"), ("STD_TREE", "VOL")]
)

plot_graph(G, 15, 15)

# --- Cálculos de la Incertidumbre

G.nodes["N_TREE"]["val"] = 6961
G.edges[("N_TREE", "VOL")]["arg"] = "n"

G.nodes["MEAN_TREE"]["val"] = 0.3106
G.edges[("MEAN_TREE", "VOL")]["arg"] = "mean"

G.nodes["STD_TREE"]["val"] = 0.009897
G.edges[("STD_TREE", "VOL")]["arg"] = "std"

G.nodes["VOL"]["unc"] = None
G.nodes["VOL"]["fun"] = uncertainty_sample

# print(f"VOL {calculate_uncertainty(G, 'VOL')}")

# ---
G.nodes["DAP"]["unc"] = 0.2

G.nodes["FEx"]["unc"] = 18

G.nodes["DM"]["unc"] = 5.6

G.nodes["BIOM_Ae_BN"]["unc"] = None
G.nodes["BIOM_Ae_BN"]["fun"] = uncertainties_product
G.nodes["BIOM_Ae_BN"]["axis"] = 0

# print(f"BIOM_Ae_BN {calculate_uncertainty(G, 'BIOM_Ae_BN')}")

# ---
G.edges[("BIOM_Ae_BN", "BIOM_Ae_Com")]["col"] = "Biomasa Aérea"

index = ["Maule", "Bío Bío", "La Araucanía", "Los Ríos", "Los Lagos"]
columns = [
    "Biomasa Aérea",
]

G.nodes["BIOM_Ae_Com"]["unc"] = None
G.nodes["BIOM_Ae_Com"]["fun"] = build_df
G.nodes["BIOM_Ae_Com"]["index"] = index
G.nodes["BIOM_Ae_Com"]["columns"] = columns

print(f"BIOM_Ae_Com {calculate_uncertainty(G, 'BIOM_Ae_Com')}")
# ---
G.nodes["BIOM_Ae_Com"]["val"] = pd.read_csv(
    os.path.join(DATA_DIR, "biomasa_aerea.csv"),
    index_col=0,
    names=["Biomasa Aérea"],
    header=0
)
# ---

G.nodes["NECR_Pie"]["unc"] = 28.4
G.edges[("NECR_Pie", "NECR_Com")]["col"] = "Necromasa Pie"

G.nodes["NECR_Suelo"]["unc"] = 24.17
G.edges[("NECR_Suelo", "NECR_Com")]["col"] = "Necromasa Suelo"


index = ["Maule", "Bío Bío", "La Araucanía", "Los Ríos", "Los Lagos"]
columns = [
    "Necromasa Pie",
    "Necromasa Suelo",
]

G.nodes["NECR_Com"]["unc"] = None
G.nodes["NECR_Com"]["fun"] = build_df
G.nodes["NECR_Com"]["index"] = index
G.nodes["NECR_Com"]["columns"] = columns

print(f"NECR_Com {calculate_uncertainty(G, 'NECR_Com')}")
# ---
G.nodes["NECR_Com"]["val"] = pd.read_csv(
    os.path.join(DATA_DIR, "necromasa_com.csv"),
    index_col=0
)
# ---

G.nodes["BIOM_Com"]["unc"] = None
G.nodes["BIOM_Com"]["fun"] = concat_dfs2
G.nodes["BIOM_Com"]["axis"] = 1

print(f"BIOM_Com {calculate_uncertainty(G, 'BIOM_Com')}")

# ---

G.nodes["M_Com"]["unc"] = None
G.nodes["M_Com"]["fun"] = uncertainties_sum
# print(f"M_Com {calculate_uncertainty(G, 'M_Com')}")

# ---

G.nodes["Em_CH4"]["unc"] = 29
G.edges[("Em_CH4", "Gases")]["col"] = "CH4"

G.nodes["Em_N2O"]["unc"] = 43.75
G.edges[("Em_N2O", "Gases")]["col"] = "N2O"

index = ["Maule", "Bío Bío", "La Araucanía", "Los Ríos", "Los Lagos"]
columns = [
    "CH4",
    "N2O",
]

G.nodes["Gases"]["unc"] = None
G.nodes["Gases"]["fun"] = build_df
G.nodes["Gases"]["index"] = index
G.nodes["Gases"]["columns"] = columns

# print(f"Gases {calculate_uncertainty(G, 'Gases')}")

# ---

G.nodes["Gases"]["val"] = pd.read_csv(
    os.path.join(DATA_DIR, "gases_regiones.csv"), index_col=0
)

G.nodes["FE_Gases"]["unc"] = None
G.nodes["FE_Gases"]["fun"] = uncertainties_sum

# print(f"FE_Gases {calculate_uncertainty(G, 'FE_Gases')}")

# ---
G.nodes["Com_Reg"]["unc"] = 36
G.edges[("Com_Reg", "Com_Reg_T")]["col"] = "Combustión"

index = ["Maule", "Bío Bío", "La Araucanía", "Los Ríos", "Los Lagos"]
columns = [
    "Combustión",
]

G.nodes["Com_Reg_T"]["unc"] = None
G.nodes["Com_Reg_T"]["fun"] = build_df
G.nodes["Com_Reg_T"]["index"] = index
G.nodes["Com_Reg_T"]["columns"] = columns

# print(f"Com_Reg_T {calculate_uncertainty(G, 'Com_Reg_T')}")

# ---

G.nodes["F_Com"]["unc"] = None
G.nodes["F_Com"]["fun"] = uncertainties_product

# print(f"F_Com {calculate_uncertainty(G, 'F_Com')}")

# ---

G.nodes["F_Com"]["val"] = G.nodes["BIOM_Ae_Com"]["val"] # copy val.

G.nodes["Area_RE"]["unc"] = None
G.nodes["Area_RE"]["fun"] = uncertainties_sum
G.nodes["Area_RE"]["axis"] = 0

print(f"Area_RE {calculate_uncertainty(G, 'Area_RE')}")

# ---

G.nodes["Sup_INC"]["unc"] = 15

G.nodes["INC"]["unc"] = None
G.nodes["INC"]["fun"] = uncertainties_product
G.nodes["INC"]["axis"] = 0

print(f"INC {calculate_uncertainty(G, 'INC')}")

