#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Propagación de Incertidumbre
Conservación

Created on Thu Oct 22 14:40:11 2020

@author: fpalm
"""

from uncertainty_propagation import *

G = nx.DiGraph()

G.add_node("CON")

G.add_nodes_from(["CON_Em", "CON_Rem"])

G.add_edges_from([("CON_Em", "CON"), ("CON_Rem", "CON")])

# ---
G.add_nodes_from(["Area_BP", "Area_BP_RP"])
G.add_edge("Area_BP_RP", "Area_BP")

# ---
G.add_node("Area_EmCON")
G.add_edge("Area_BP", "Area_EmCON")

# ---
G.add_node("CON_C") # Existencia de Carbono en áreas de conservación

G.add_edges_from(
    [
        ("CON_C", "CON_Rem"),
        ("Area_EmCON", "CON_Rem")
    ]
)

G.add_edges_from(
    [
        ("CON_C", "CON_Em"),
        ("Area_EmCON", "CON_Em")
    ]
)

G.add_nodes_from(["DAP", "FEx", "DM"])
G.add_edges_from(
    [
        ("DAP", "CON_C"),
        ("FEx", "CON_C"),
        ("DM", "CON_C"),
    ]
)

G.add_node("VKNN")
G.add_nodes_from(["N_VKNN", "MEAN_VKNN", "STD_VKNN"])
G.add_edges_from([
    ("N_VKNN", "VKNN"),
    ("MEAN_VKNN", "VKNN"),
    ("STD_VKNN", "VKNN"),
])

G.add_edge("VKNN", "CON_C")

G.add_node("SAT")
G.add_nodes_from(["N_SAT", "MEAN_SAT", "STD_SAT"])
G.add_edges_from([
    ("N_SAT", "SAT"),
    ("MEAN_SAT", "SAT"),
    ("STD_SAT", "SAT"),
])

G.add_node("AB")
G.add_nodes_from(["N_AB", "MEAN_AB", "STD_AB"])
G.add_edges_from([
    ("N_AB", "AB"),
    ("MEAN_AB", "AB"),
    ("STD_AB", "AB"),
])

G.add_node("PCD") # Precisión de la cartografía de degradación
G.add_edges_from([
    ("PCD", "Area_EmCON"),
    ("AB", "Area_EmCON"),
    ("SAT", "Area_EmCON"),
])

plot_graph(G, 15, 15)

# --- Cálculos de la Incertidumbre

G.nodes["N_AB"]["val"] = 3152
G.edges[("N_AB", "AB")]["arg"] = "n"

G.nodes["MEAN_AB"]["val"] = 29
G.edges[("MEAN_AB", "AB")]["arg"] = "mean"

G.nodes["STD_AB"]["val"] = 13.7
## Utilizan el error cuadrático medio, considerando el sesgo.
G.edges[("STD_AB", "AB")]["arg"] = "std"

G.nodes["AB"]["unc"] = None
G.nodes["AB"]["fun"] = uncertainty_sample

# print(f"AB {calculate_uncertainty(G, 'AB')}")
# ---

G.nodes["N_VKNN"]["val"] = 417
G.edges[("N_VKNN", "VKNN")]["arg"] = "n"

G.nodes["MEAN_VKNN"]["val"] = 320.52
G.edges[("MEAN_VKNN", "VKNN")]["arg"] = "mean"

G.nodes["STD_VKNN"]["val"] = 2280.7743408105944
# 111.69 * 417**0.5
G.edges[("STD_VKNN", "VKNN")]["arg"]= "std"

G.nodes["VKNN"]["unc"] = None
G.nodes["VKNN"]["fun"] = uncertainty_sample

# print(f"VKNN {calculate_uncertainty(G, 'VKNN')}")
# ---

G.nodes["DAP"]["unc"] = 0.2

G.nodes["FEx"]["unc"] = 18

G.nodes["DM"]["unc"] = 5.6

G.nodes["CON_C"]["unc"] = None
G.nodes["CON_C"]["fun"] = uncertainties_product
G.nodes["CON_C"]["axis"] = 0

# print(f"CON_C {calculate_uncertainty(G, 'CON_C')}")
# ---

G.nodes["Area_BP_RP"]["unc"] = pd.read_csv(
    os.path.join(DATA_DIR, "uarea_bp_rp.csv"), index_col=0
)

G.nodes["Area_BP"]["unc"] = None
G.nodes["Area_BP"]["fun"] = uncertainties_product
G.nodes["Area_BP"]["axis"] = 0

# print(f"Area_BP {calculate_uncertainty(G, 'Area_BP')}")
# ---

G.nodes["N_SAT"]["val"] = 17477169
G.edges[("N_SAT", "SAT")]["arg"] = "n"

G.nodes["MEAN_SAT"]["val"] = 16.37
G.edges[("MEAN_SAT", "SAT")]["arg"] = "mean"

G.nodes["STD_SAT"]["val"] = 16.37
G.edges[("STD_SAT", "SAT")]["arg"]= "std"

G.nodes["SAT"]["unc"] = None
G.nodes["SAT"]["fun"] = uncertainty_sample

# print(f"SAT {calculate_uncertainty(G, 'SAT')}")

# ---

G.nodes["PCD"]["unc"] = 42

G.nodes["Area_EmCON"]["unc"] = None
G.nodes["Area_EmCON"]["fun"] = uncertainties_product
G.nodes["Area_EmCON"]["axis"] = 0

# print(f"Area_EmCON {calculate_uncertainty(G, 'Area_EmCON')}")

# ---

G.nodes["CON_Em"]["unc"] = None
G.nodes["CON_Em"]["fun"] = uncertainties_product
G.nodes["CON_Em"]["axis"] = 0

# print(f"CON_Em {calculate_uncertainty(G, 'CON_Em')}")

# ---

G.nodes["CON_Rem"]["unc"] = None
G.nodes["CON_Rem"]["fun"] = uncertainties_product
G.nodes["CON_Rem"]["axis"] = 0

# print(f"CON_Rem {calculate_uncertainty(G, 'CON_Rem')}")

# ---

G.nodes["CON_Em"]["val"] = 1286581.55555556
G.nodes["CON_Rem"]["val"] = 3717019.77777778

G.nodes["CON"]["unc"] = None
G.nodes["CON"]["fun"] = uncertainties_sum
G.nodes["CON"]["axis"] = 0

print(f"CON {calculate_uncertainty(G, 'CON')}")

