#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 07:47:41 2020

@author: fpalm
"""

from uncertainty_propagation import *

df1 = pd.DataFrame([["a", 1], ["b", 2]], columns=["letter", "l-val"])
df1.set_index("letter", inplace=True)
"""
        l-val
letter       
a           1
b           2
"""
df2 = pd.DataFrame([["1", 3], ["2", 4]], columns=["digit", "d-val"])
df2.set_index("digit", inplace=True)
"""
       d-val
digit       
1          3
2          4
"""
row = [df1, df2]
result = cross_dfs(row)
"""
              d-val  l-val
digit letter              
1     a           3      1
      b           3      2
2     a           4      1
      b           4      2
"""
val = pd.DataFrame(
    [
        ["a", "1", "azul", 1],
        ["b", "1", "rojo", 2],
        ["a", "1", "rojo", 3],
        ["b", "2", "azul", 4],
        ["a", "2", "rojo", 5],
        ["b", "1", "rojo", 6],
    ],
    columns=["i1", "i2", "i3", "val"],
)
val.set_index(["i1", "i2"], inplace=True)
"""
         i3  val
i1 i2           
a  1   azul    1
b  1   rojo    2
a  1   rojo    3
b  2   azul    4
a  2   rojo    5
b  1   rojo    6
"""
unc = pd.DataFrame(
    [
        ["a", "1", 0.1],
        ["b", "1", 0.2],
        ["a", "2", 0.3],
        ["b", "2", 0.4]
    ],
    columns=["i1", "i2", "val"],
)
unc.set_index(["i1", "i2"], inplace=True)
"""
       val
i1 i2     
a  1   0.1
b  1   0.2
a  2   0.3
b  2   0.4
"""
pval, punc = merge_dfs(val, unc, index="i3", on=["i1", "i2"])
pval
      val
i3       
azul    1
rojo    3
rojo    2
rojo    6
azul    4
rojo    5
punc
      val
i3       
azul  0.1
rojo  0.1
rojo  0.2
rojo  0.2
azul  0.4
rojo  0.3

