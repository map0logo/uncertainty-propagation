#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Propagación de Incertidumbre
Deforestación

Created on Sun Oct 18 18:18:47 2020

@author: fpalm
"""

from uncertainty_propagation import *

G = nx.DiGraph()
# --- Nodo Final, Deforestación
G.add_node("DEF")
G.add_node("DEF_Em")
G.add_edge("DEF_Em", "DEF")
# ---
G.add_nodes_from(["DEF_DA", "DEF_FE", "DEF_MA_T"])
G.add_edges_from(
    [("DEF_DA", "DEF_Em"), ("DEF_FE", "DEF_Em"), ("DEF_MA_T", "DEF_Em")]
)
# ---
G.add_node("DA")
G.add_edge("DA", "DEF_DA")
# ---
G.add_node("DEF_MA")
G.add_edge("DEF_MA", "DEF_MA_T")
# ---
G.add_node("BIOM_MA")
G.add_edge("BIOM_MA", "DEF_MA")
# ---
G.add_node("BIOM")
G.add_edge("BIOM", "DEF_FE")
# ---
G.add_nodes_from(["NECR_Pie", "NECR_Suelo"])
G.add_edges_from([("NECR_Pie", "BIOM"), ("NECR_Suelo", "BIOM")])
# ---
G.add_node("BIOM_Sub_MA")
G.add_edge("BIOM_Sub_MA", "DEF_MA")
# ---
G.add_nodes_from(["BIOM_Ae_MA", "FR_MA"])
G.add_edges_from([("BIOM_Ae_MA", "BIOM_Sub_MA"), ("FR_MA", "BIOM_Sub_MA")])
# ---
G.add_node("BIOM_Sub_BN")
G.add_edge("BIOM_Sub_BN", "BIOM")

# ---
G.add_nodes_from(["BIOM_Ae_BN", "FR_BN"])
G.add_edges_from(
    [
        ("BIOM_Ae_BN", "BIOM_Sub_BN"),
        ("FR_BN", "BIOM_Sub_BN"),
        ("BIOM_Ae_BN", "BIOM"),
    ]
)
# ---
G.add_nodes_from(["DAP", "VOL", "FEx", "DM"])
G.add_edges_from(
    [
        ("DAP", "BIOM_Ae_BN"),
        ("VOL", "BIOM_Ae_BN"),
        ("FEx", "BIOM_Ae_BN"),
        ("DM", "BIOM_Ae_BN"),
    ]
)
# ---
G.add_nodes_from(["N_TREE", "MEAN_TREE", "STD_TREE"])
G.add_edges_from(
    [("N_TREE", "VOL"), ("MEAN_TREE", "VOL"), ("STD_TREE", "VOL")]
)

plot_graph(G, 15, 15)


# --- Cálculos de la Incertidumbre

G.nodes["N_TREE"]["val"] = 6961
G.edges[("N_TREE", "VOL")]["arg"] = "n"

G.nodes["MEAN_TREE"]["val"] = 0.3106
G.edges[("MEAN_TREE", "VOL")]["arg"] = "mean"

G.nodes["STD_TREE"]["val"] = 0.009897
G.edges[("STD_TREE", "VOL")]["arg"] = "std"

G.nodes["VOL"]["unc"] = None
G.nodes["VOL"]["fun"] = uncertainty_sample
# print(f"VOL {calculate_uncertainty(G, 'VOL')}")

# ---
G.nodes["DAP"]["unc"] = 0.2

G.nodes["FEx"]["unc"] = 18

G.nodes["DM"]["unc"] = 5.6

G.nodes["BIOM_Ae_BN"]["unc"] = None
G.nodes["BIOM_Ae_BN"]["fun"] = uncertainties_product
G.nodes["BIOM_Ae_BN"]["axis"] = 0

# ---
G.nodes["FR_BN"]["unc"] = 40

G.nodes["BIOM_Sub_BN"]["unc"] = None
G.nodes["BIOM_Sub_BN"]["fun"] = uncertainties_product
G.nodes["BIOM_Sub_BN"]["axis"] = 0

# print(f"BIOM_Sub_BN {calculate_uncertainty(G, 'BIOM_Sub_BN')}")

# ---
G.nodes["BIOM_Ae_MA"]["unc"] = 22.4225

G.nodes["FR_MA"]["unc"] = 48.27

G.nodes["BIOM_Sub_MA"]["unc"] = None
G.nodes["BIOM_Sub_MA"]["fun"] = uncertainties_product
G.nodes["BIOM_Sub_MA"][
    "axis"
] = 0  # para que aplique la función como una fila.

# print(f"BIOM_Sub_MA {calculate_uncertainty(G, 'BIOM_Sub_MA')}")

# ---
G.edges[("BIOM_Ae_BN", "BIOM")]["col"] = "Biomasa Aérea"

G.edges[("BIOM_Sub_BN", "BIOM")]["col"] = "Biomasa Subterránea"

# Error estimado de las parcelas permanentes del Inventario forestal continúo del INFOR.
G.nodes["NECR_Pie"]["unc"] = 28.4
G.edges[("NECR_Pie", "BIOM")]["col"] = "Necromasa Pie"

# Error estimado de las parcelas permanentes del Inventario forestal continúo del INFOR.
G.nodes["NECR_Suelo"]["unc"] = 24.17
G.edges[("NECR_Suelo", "BIOM")]["col"] = "Necromasa Suelo"


index = ["Maule", "Bío Bío", "La Araucanía", "Los Ríos", "Los Lagos"]
columns = [
    "Biomasa Aérea",
    "Biomasa Subterránea",
    "Necromasa Pie",
    "Necromasa Suelo",
]

G.nodes["BIOM"]["unc"] = None
G.nodes["BIOM"]["fun"] = build_df
G.nodes["BIOM"]["index"] = index
G.nodes["BIOM"]["columns"] = columns

# print(f"BIOM {calculate_uncertainty(G, 'BIOM')}")

# ---
G.nodes["BIOM"]["val"] = pd.read_csv(
    os.path.join(DATA_DIR, "biomasa.csv"), index_col=0
)
G.edges[("BIOM", "DEF_FE")]["arg"] = "values"

G.nodes["DEF_FE"]["unc"] = None
G.nodes["DEF_FE"]["fun"] = uncertainties_sum
G.nodes["DEF_FE"]["type"] = "df"
G.nodes["DEF_FE"]["axis"] = 1

# print(f"DEF_FE {calculate_uncertainty(G, 'DEF_FE')}")

# ---
# Gayoso (2006)
G.nodes["BIOM_MA"]["val"] = 21.68
G.nodes["BIOM_MA"]["unc"] = 22.4225
# G.edges[("BIOM_MA", "DEF_MA")]["arg"] = "values"

G.nodes["BIOM_Sub_MA"]["val"] = 35.25
# G.edges[("BIOM_Sub_MA", "DEF_MA")]["arg"] = "values"

G.nodes["DEF_MA"]["unc"] = None
G.nodes["DEF_MA"]["fun"] = uncertainties_sum
G.nodes["DEF_MA"]["axis"] = 0

# print(f"DEF_MA {calculate_uncertainty(G, 'DEF_MA')}")

# ---
G.nodes["DA"]["val"] = pd.read_csv(
    os.path.join(DATA_DIR, "area_def.csv"), index_col=0
)
G.nodes["DA"]["unc"] = pd.read_csv(
    os.path.join(DATA_DIR, "uarea_def.csv"), index_col=0
)

G.nodes["DEF_DA"]["unc"] = None
G.nodes["DEF_DA"]["fun"] = uncertainties_sum
G.nodes["DEF_DA"]["axis"] = 1

# print(f"DEF_DA {calculate_uncertainty(G, 'DEF_DA')}")
# ---

index = ["Maule", "Bío Bío", "La Araucanía", "Los Ríos", "Los Lagos"]
columns = ["Matorral Arborescente"]
G.nodes["DEF_MA_T"]["unc"] = None
G.nodes["DEF_MA_T"]["fun"] = build_df
G.nodes["DEF_MA_T"]["index"] = index
G.nodes["DEF_MA_T"]["columns"] = columns

G.edges[("DEF_MA", "DEF_MA_T")]["col"] = "Matorral Arborescente"

# print(f"DEF_MA_T {calculate_uncertainty(G, 'DEF_MA_T')}")
# ---

G.nodes["DEF_Em"]["unc"] = None
G.nodes["DEF_Em"]["fun"] = uncertainties_product
G.nodes["DEF_Em"]["axis"] = 1

# print(f"DEF_Em {calculate_uncertainty(G, 'DEF_Em')}")
# ---

G.nodes["DEF_Em"]["val"] = pd.read_csv(
    os.path.join(DATA_DIR, "emisiones_def.csv"),
    header=0,
    names=[0],
    index_col=0,
)

G.nodes["DEF"]["unc"] = None
G.nodes["DEF"]["fun"] = uncertainties_sum
G.nodes["DEF"]["axis"] = 0

print(f"DEF {calculate_uncertainty(G, 'DEF')}")
